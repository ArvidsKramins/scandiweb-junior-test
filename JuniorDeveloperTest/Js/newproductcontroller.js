var diskAttr = `
<div class="col-md-7 p-0" id="disc-card">
  <div class="card ">
    <div class="card-body">
      <div class="form-group row">
        <label class="col-sm-5 col-form-label">Size</label>
          <input type="text" class="form-control col-md-7" name="attribute" id="attribute">
          <input type="hidden" class="form-control col-md-7" name="type" id="type" value="1">
      </div>
      <p class="card-text text-center">Please provide Disk size in MB  ex:700</p>      </div>
  </div>
</div>`;
var bookAtt = `
<div class="col-md-7 p-0" id="disc-card">
  <div class="card ">
    <div class="card-body">
      <div class="form-group row">
        <label class="col-sm-5 col-form-label">Weight</label>
        <input type="hidden" class="form-control col-md-7" name="type" id="type" value="2">
          <input type="text" class="form-control col-md-7" name="attribute" id="attribute">
      </div>
      <p class="card-text text-center">Please provide Book weight in KG  ex: 0,8 </p>      </div>
  </div>
</div>`;
var furnAtt = `
<div class="col-md-7 p-0" id="disc-card">
  <div class="card ">
    <div class="card-body">
      <div class="form-group row">
        <label class="col-sm-5 col-form-label">Dimensions</label>
          <input type="hidden" class="form-control col-md-7" name="type" id="type" value="3">
          <input type="text" class="form-control col-md-7" name="attribute" id="attribute">
      </div>
      <p class="card-text text-center">Please provide furniture dimension in HxWxL </p>      </div>
  </div>
</div>`;

$('#input-product-type').on('change', function(e, data) {
  var input = $(this).val();
  switch (input) {
    case "DISC":
      $("#attribute-holder").empty();
      $("#attribute-holder").append(diskAttr);
      break;
    case "BOOK":
      $("#attribute-holder").empty();
      $("#attribute-holder").append(bookAtt);
      break;
    case "FURNITURE":
      $("#attribute-holder").empty();
      $("#attribute-holder").append(furnAtt);
      break;
  }
});
$.fn.myfunction = function () {
    console.log(this.length);
};
// Input Controlls && ERROR MESSAGE HANDLER
$(document).ready(function() {
  var formIsOk;
  $('#new-product-form').submit(function(e) {
    formIsOk = true;
    var sku = $("#sku").val();
    var name = $("#name").val();
    var price = $("#price").val();
    var attr = $("#attribute").val();
    $(".error").remove();
    //INPUT SKU - HANDLER
    if (sku.length < 1) {
      $('#sku').after('<div class="col-md-5 text-center error col-form-label"> <label class="text-danger">Must be 8-20 characters long.</label></div>');
      formIsOk = false;
    } else {
      $.ajaxSetup({
        async: false
      });
      $.get("classes/formcontroller.class.php", {
        'name': 'getSKU',
        'sku': sku
      }, function(data) {
        if (data == true) {
          formIsOk = false;
          console.log("insaid function" + formIsOk);
          $('#sku').after('<div class="col-md-5 text-center error col-form-label"> <label class="text-danger">SKU Already exists</label></div>');

        }
      });
    }
      //INPUT NAME - HANDLER
    if (name.length < 1) {
      formIsOk = false;
      $('#name').after('<div class="col-md-5 text-center error col-form-label"> <label class="text-danger">Must be 8-20 characters long.</label></div>');
    }

      //INPUT PRICE - HANDLER
    if (price.length < 1) {
      formIsOk = false;
      $('#price').after('<div class="col-md-5 text-center error col-form-label"> <label class="text-danger">Must be 8-20 characters long.</label></div>');
    }else {
      if (price > 1000 || price<0 ) {
        $('#price').after('<div class="col-md-5 text-center error col-form-label"> <label class="text-danger">Price must be between 0 and 999.99</label></div>');
      }
    }
      //INPUT ATTRIBUTE - HANDLER
    if (attr.length < 1) {
      formIsOk = false;
      $('#attribute').after('<div class="error col-form-label"> <label class="text-danger">Must be 8-20 characters long.</label></div>');
    }
    return formIsOk;
  });
});
