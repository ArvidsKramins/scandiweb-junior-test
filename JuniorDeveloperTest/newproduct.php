<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Product List</title>
  </head>



  <body>
    <div class="container">
      <form action="classes/formcontroller.class.php"  id="new-product-form" method="post">
      <div class="row pt-2 justify-content-between">
        <div class="col-3">
          <h1>Product List</h1>
        </div>


        <div class="colfloat-right pt-2">
            <button type="submit" name="createProduct" class="btn btn-primary">Submit</button>
        </div>
      </div>
      <hr>
      <div class="col-sm-12 col-md-8">
        <div class="form-group row">
          <label class="col-md-3 col-form-label">SKU <i class="fa fa-info-circle " data-toggle="tooltip" data-trigger="hover focus" data-placement="right" title="Stock Keeping Unit" aria-hidden="true"></i></label>
          <input type="text" class="form-control col-md-4" name="SKU" id="sku">
        </div>
        <div class="form-group row">
          <label class="col-sm-3 col-form-label">Name</label>
            <input type="text" class="form-control col-md-4" name="name" id="name">
        </div>
        <div class="form-group row">
          <label class="col-sm-3 col-form-label">Price</label>
            <input type="text" class="form-control col-md-4" name="price" id="price">
        </div>
        <div class="form-group row">
          <label class="col-sm-4 col-form-label">Type Switcher</label>
          <select class="custom-select col-md-3" id="input-product-type">
            <option selected>DISC</option>
            <option>BOOK</option>
            <option>FURNITURE</option>
          </select>
          </div>
          <div id="attribute-holder">
            <div class="col-md-7 p-0" id="disc-card">
              <div class="card ">
                <div class="card-body">
                  <div class="form-group row">
                    <label class="col-sm-5 col-form-label">Size</label>
                    <input type="text" class="form-control col-md-7" name="attribute" id="attribute">
                    <input type="hidden" class="form-control col-md-7" name="type" id="type" value="1">                  </div>
                  <p class="card-text text-center">Please provide Disk size in MB  ex:700</p>      </div>
              </div>
            </div>

          </div>

  </div>

  </div>
    </form>

    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

    <script  src="https://code.jquery.com/jquery-3.4.1.js"  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="  crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="Js/newproductcontroller.js"></script>
  </body>
</html>
