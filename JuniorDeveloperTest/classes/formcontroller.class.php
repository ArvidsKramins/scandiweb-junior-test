<?php
/*
* formcontroller ->> INTERACTS with form.
* USED FOR POSTNG AND GETTING FROM DATABASE USNG FORM
*/
include "dbh.class.php";
include "product.class.php";
include "productcontroller.class.php";
include "productview.class.php";

//CREATE PRODUCT FORM HANDLER
if (isset($_POST['createProduct'])) {
  $SKU = $_POST['SKU'];
  $name = $_POST['name'];
  $price = $_POST['price'];
  $att = $_POST['attribute'];
  $prodType = $_POST['type'];
  $prodCont = new ProductController();
  $prodCont->createProduct($SKU, $name,$price,$att,$prodType);
  header("Location: /index.php");
  exit();
}
//DELETE PRODUCT FORM HANDLER
if (isset($_POST['delete'])) {
  $arr = $_POST['checkbox'];
  $prodCont = new ProductController();
  foreach ($arr as $SKU) {
    $prodCont->deleteProduct($SKU);
  }
  header("Location: /index.php");
  exit();
}
//CHECK IF SKU EXIST PRODUCT FORM HANDLER
if ($_GET['name'] = "getSKU") {
    $returnVal;
    $prodCont = new productView();
    $results = $prodCont->selectProduct($_GET['sku']);
    echo $results;
    exit();
}
else {
   echo "FORM REQUEST TO FORMCONTROLLER WAS NOT SET PROPERTLY";
}
 ?>
