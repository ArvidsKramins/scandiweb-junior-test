<?php
/**
 * Product class derives from DBH to DB connection.
 * Product class is standard MVC design pattern model class.
 * It is used to manage all DB interfaces and data flow.
 */
class Product extends DBH
{
    //Returns all products from DB.
    protected function getProducts(){
      $sql = "SELECT * FROM products";
      $stmt = $this->connect()->prepare($sql);
      $stmt->execute([]);
      $results = $stmt->fetchAll();
      return  $results;
    }

    //Returns all products from DB.
    protected function getProduct($SKU){
      $sql = "SELECT * FROM products where SKU = ?";
      $stmt = $this->connect()->prepare($sql);
      $stmt->execute([$SKU]);
      $results = $stmt->fetchAll();
      return  $results;
    }

    //Inserts Record into DB With given parameters.
    protected function insertProduct($SKU,$NAME, $PRICE, $ATTRIBUTE,$PRODUCT_TYPE_ID){
      $sql = "INSERT INTO products(SKU,NAME,PRICE,ATTRIBUTE,PRODUCT_TYPE_ID) VALUES(?,?,?,?,?)";
      $stmt = $this->connect()->prepare($sql);
      $stmt->execute([$SKU,$NAME, $PRICE, $ATTRIBUTE,$PRODUCT_TYPE_ID]);
    }

    //Deletes Record from product table with given ID
    protected function delete($ID){
      $sql = "DELETE from products where ID = ?";
      $stmt = $this->connect()->prepare($sql);
      $stmt->execute([$ID]);
    }
}
 ?>
