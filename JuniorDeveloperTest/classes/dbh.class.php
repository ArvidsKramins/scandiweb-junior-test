<?php
/*
* DataBase Handler Class
* Purpose: This is base class for all other classes that are using database connection.
* This CLASS creates Database  connection and returns PDO
*/
  class DBH
  {
    private $servername;
    private $username  ;
    private $password;
    private $dbname;
    private $charset;
    public function connect(){
      $this->servername = "localhost";
      $this->username = "root";
      $this->password = "root";
      $this->dbname = "products";
      $this->charset = "utf8mb4";
      try {
        $dsn = "mysql:host=".$this->servername.";dbname=".$this->dbname.";charset=". $this->charset;
        $pdo = new PDO($dsn,$this->username,$this->password);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        return $pdo;
      } catch (PDOException $e) {
        echo "error" . $e->getMessage();
      }
    }
  }
?>
