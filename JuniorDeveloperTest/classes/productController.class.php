<?php

/**
 * This is class is standart MVC model class.
 * Product class is standard MVC design pattern model class.
 * It is used to manage all DB interfaces and data flow.
 */
class ProductController extends Product
{
    public function createProduct($SKU,$NAME, $PRICE, $ATTRIBUTE,$PRODUCT_TYPE_ID){
      $this->insertProduct($SKU,$NAME, $PRICE, $ATTRIBUTE,$PRODUCT_TYPE_ID);
    }
    public function updateProduct($SKU, $NAME, $PRICE, $ATTRIBUTE,$PRODUCT_TYPE_ID){
      $this->update($SKU,$NAME, $PRICE, $ATTRIBUTE,$PRODUCT_TYPE_ID);
    }
    public function deleteProduct($ID){
      $this->delete($ID);
    }
}

 ?>
