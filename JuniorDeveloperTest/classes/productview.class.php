<?php
/**
 * This is class is standart MVC VIEW class.
 * It is used to CREATE VIEWS, THAT JUSTS SELECTS FROM DB
 */
class ProductView extends Product
{

   public function selectProducts(){
     $results = $this->getProducts();
     foreach($results as $result) {
       echo '<div class="col-md-3">
             <div class="card mb-3 box-shadow">
               <div class="card-body">
                 <input type="checkbox" name="checkbox[]" value="' .$result['ID'].'">
                 <p class="card-text text-center">'.$result['SKU'].'</p>
                 <p class="card-text text-center">'.$result['NAME'].'</p>
                 <p class="card-text text-center">'.$result['PRICE'].'</p>
                 <p class="card-text text-center">';
                 switch ($result['PRODUCT_TYPE_ID']) {
                   case 1:
                    echo 'SIZE :';
                     break;
                   case 2:
                    echo 'Weight :';
                     break;
                   case 3:
                    echo 'Dimensions :';
                     break;
                   default:
                     echo 'Attribute :';
                     break;
                     }
                     echo $result['ATTRIBUTE'].'</p>
                 </item>
               </div>
             </div>
           </div>';

    }
  }
      public function selectProduct($SKU){
        $results = $this->getProduct($SKU);
        $returns = true;
        if (count($results)==0) {
          $returns = false;
        }
        return $returns;
    }
}


 ?>
